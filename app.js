const express = require("express");
const bodyParser = require("body-parser");

const userRouter = require("./routes/userRoutes");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/users", userRouter);

app.listen(3000, () => {
  console.log("Server is runing");
});
