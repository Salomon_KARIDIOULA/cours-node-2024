const connection = require("../config/db");

const getUsers = (req, res) => {
  connection.query("SELECT * FROM user", (err, rows) => {
    if (err) {
      console.log(
        "Une erreur est servenue lors de la recupération des utilisateurs"
      );
    }

    res.json(rows);
  });
};

const getUserById = (req, res) => {
  const { id } = req.params;

  connection.query("SELECT * FROM user WHERE id = ?", [+id], (err, rows) => {
    if (err) {
      console.log("Une erreur est servenue");
    }

    if (rows.length <= 0) {
      res.json({
        message: "Aucun utilisateur trouvé avec l'id: " + id,
      });
    }

    res.json(rows[0]);
  });
};

const createUser = (req, res) => {
  const { nom, email, password } = req.body;

  connection.query(
    "INSERT INTO user (nom, email, password) VALUES (?, ?, ?)",
    [nom, email, password],
    (err, result) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          statusCode: 500,
          message: "Une erreur est survenue",
        });
      }

      res.json({
        message: "Utilisateur crée avec succès",
      });
    }
  );
};

const updateUser = (req, res) => {
  const { nom, email, password } = req.body;
  const { id } = req.params;

  if (!nom || !email || !password) {
    res.json({
      error: true,
      message: "Veuillez renseigner l'email, le nom et le mot de passe",
    });
  }

  connection.query(
    "UPDATE user SET nom = ?, email = ?,  password = ? WHERE id = ?",
    [nom, email, password, +id],
    (err, result) => {
      if (err) {
        console.log(err);
        res.status(400).json({
          statusCode: 400,
          message: "Une erreur est survenue",
        });
      }

      res.json({
        message: "Utilisateur modifié avec succès",
      });
    }
  );
};

const deleteUser = (req, res) => {
  connection.query(
    "DELETE FROM user WHERE id = ?",
    [+req.params.id],
    (err, result) => {
      if (err) throw err;

      res.json({ message: "Utilisateur supprimé avec succès." });
    }
  );
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
